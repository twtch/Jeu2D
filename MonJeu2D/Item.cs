﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonJeu2D {
    class Item {
        //CHAMPS
        Rectangle hitbox;
        int number;
        int quantity;
        int maxQuantity;

        SpriteFont fontText;

        //CONSTRUCTEUR
        public Item(int NUMBER) {
            number = NUMBER;
            hitbox = new Rectangle(200, 200, 99 / 4, 148 / 4 );
            quantity = 99;
            maxQuantity = 10;
        }

        //METHODE

        //UPDATE & DRAW
        public void Update() {

            if (quantity >= maxQuantity) {
                quantity = maxQuantity;
            }
            if (quantity < 0) {
                quantity = 0;
            }

        }

        public void Draw(SpriteBatch spriteBatch) {
            spriteBatch.DrawString(Ressource.fontText, "" + quantity, new Vector2(hitbox.X + hitbox.Width, hitbox.Y), Color.Black);
            spriteBatch.Draw(Ressource.listPotion[number], hitbox, Color.White);
        }

    }
}
