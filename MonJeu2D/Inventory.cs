﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonJeu2D {
    class Inventory {
        //CHAMPS
        Rectangle hitbox;
        Vector2 position;

        public Object[] array;

        //CONSTRUCTEUR
        public Inventory() {
            position = new Vector2();
            array = new Object[9];

            hitbox = new Rectangle(15, 250, 64, 64);
        }

        //METHODE

        //UPDATE ET DRAW
        public void Update() {

        }

        
        public void Draw(SpriteBatch spriteBatch) {
            int xx = 0;
            int yy = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 4; j++) {
                    spriteBatch.Draw(Ressource.caseInventaire, new Rectangle(this.hitbox.X + xx, this.hitbox.Y + yy, this.hitbox.Width, this.hitbox.Height), Color.GhostWhite);
                    xx += 67;
                }
                xx = 0;
                yy += 67;
            }
        }
    }
}
