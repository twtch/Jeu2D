﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonJeu2D {
    class Player {
        //CHAMPS
        public int Skin { get; set; }
        Rectangle hitbox;

        int frameLine;
        int frameColumn;

        int speed = 2;

        Direction direction;
        bool animation;

        int timer;
        int animationSpeedCoursePlayer = 7;

        //CONSTRUCTEUR
        public Player(int skin) {
            this.Skin = skin;
            this.frameLine = 1;
            this.frameColumn = 2;
            this.direction = Direction.DOWN;
            this.animation = true;
            this.timer = 0;
            hitbox = new Rectangle(0, 0, 32, 47);
        }
        //METHODE
        public void animate() {
            this.timer++;
            if(this.timer == this.animationSpeedCoursePlayer) {
                timer = 0;
                if (this.animation) {
                    this.frameColumn++;
                    if (this.frameColumn > 3) {
                        this.frameColumn = 2;
                        this.animation = false;
                    }
                } else {
                    this.frameColumn--;
                    if (this.frameColumn < 1) {
                        this.frameColumn = 2;
                        this.animation = true;
                    }
                }
            }

        }

        //UPDATE & DRAW
        public void Update(MouseState mouse, KeyboardState keyboard, List<Wall> walls) {
            if (keyboard.IsKeyDown(Keys.Up) || keyboard.IsKeyDown(Keys.Z)) {

                Rectangle newHitbox = new Rectangle(this.hitbox.X, this.hitbox.Y - this.speed, this.hitbox.Width, this.hitbox.Height);
                bool collide = false;
                foreach (Wall item in walls) {
                    if (newHitbox.Intersects(item.hitbox)) {
                        collide = true;
                        break;
                    }
                }

                if (!collide) {
                    this.hitbox.Y -= speed;
                    animate();
                }
                this.direction = Direction.UP;
                
            }
            else if (keyboard.IsKeyDown(Keys.Down) || keyboard.IsKeyDown(Keys.S)) {
                Rectangle newHitbox = new Rectangle(this.hitbox.X, this.hitbox.Y + this.speed, this.hitbox.Width, this.hitbox.Height);
                bool collide = false;
                foreach (Wall item in walls) {
                    if (newHitbox.Intersects(item.hitbox)) {
                        collide = true;
                        break;
                    }
                }

                if (!collide) {
                    this.hitbox.Y += speed;
                    animate();
                }
                this.direction = Direction.DOWN;
                
            }
            else if (keyboard.IsKeyDown(Keys.Left) || keyboard.IsKeyDown(Keys.Q)) {
                Rectangle newHitbox = new Rectangle(this.hitbox.X - this.speed, this.hitbox.Y, this.hitbox.Width, this.hitbox.Height);
                bool collide = false;
                foreach (Wall item in walls) {
                    if (newHitbox.Intersects(item.hitbox)) {
                        collide = true;
                        break;
                    }
                }

                if (!collide) {
                    this.hitbox.X -= speed;
                    animate();
                }
                this.direction = Direction.LEFT;
                
            }
            else if (keyboard.IsKeyDown(Keys.Right) || keyboard.IsKeyDown(Keys.D)) {
                Rectangle newHitbox = new Rectangle(this.hitbox.X + this.speed, this.hitbox.Y, this.hitbox.Width, this.hitbox.Height);
                bool collide = false;
                foreach (Wall item in walls) {
                    if (newHitbox.Intersects(item.hitbox)) {
                        collide = true;
                        break;
                    }
                }

                if (!collide) {
                    this.hitbox.X += speed;
                    animate();
                }
                this.direction = Direction.RIGHT;
                
            }

            if (keyboard.IsKeyUp(Keys.Up) && keyboard.IsKeyUp(Keys.Down) && keyboard.IsKeyUp(Keys.Left) && keyboard.IsKeyUp(Keys.Right)
                && keyboard.IsKeyUp(Keys.Z) && keyboard.IsKeyUp(Keys.S) && keyboard.IsKeyUp(Keys.Q) && keyboard.IsKeyUp(Keys.D)) {
                this.frameColumn = 2;
                this.timer = 0;
            }

            
            switch (this.direction) {
                case Direction.UP:
                    this.frameLine = 4;
                    break;
                case Direction.DOWN:
                    this.frameLine = 1;
                    break;
                case Direction.LEFT:
                    this.frameLine = 2;
                    break;
                case Direction.RIGHT:
                    this.frameLine = 3;
                    break;
                default:
                    break;
            }


        }

        public void Draw(SpriteBatch spriteBatch) {
            spriteBatch.DrawString(Ressource.fontText, "POSITION JOUEUR \n: X: " + this.hitbox.X + ", Y: " + this.hitbox.Y, new Vector2(20, 20), Color.Black);
            spriteBatch.Draw(Ressource.playerSkin[Skin], hitbox, new Rectangle((frameColumn - 1) * 32, (frameLine - 1) * 47, 32, 47), Color.White);
        }

    }
}
