﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonJeu2D {
    class Ressource {
        //CHAMPS
        public static List<Texture2D> playerSkin;
        public static Texture2D pixel;
        public static List<Texture2D> listPotion;
        public static SpriteFont fontText;
        public static Texture2D caseInventaire;

        public static void LoadContent(ContentManager content) {
            fontText = content.Load<SpriteFont>("SpriteFont");

            caseInventaire = content.Load<Texture2D>("img/Case-grise-inventaire");

            playerSkin = new List<Texture2D>();
            playerSkin.Add(content.Load<Texture2D>("img/SpriteSheet1"));
            playerSkin.Add(content.Load<Texture2D>("img/SpriteSheet2"));
            playerSkin.Add(content.Load<Texture2D>("img/SpriteSheet3"));
            playerSkin.Add(content.Load<Texture2D>("img/SpriteSheet4"));
            playerSkin.Add(content.Load<Texture2D>("img/SpriteSheet5"));
            playerSkin.Add(content.Load<Texture2D>("img/SpriteSheet6"));
            playerSkin.Add(content.Load<Texture2D>("img/SpriteSheet7"));
            playerSkin.Add(content.Load<Texture2D>("img/SpriteSheet8"));

            pixel = content.Load<Texture2D>("img/Pixel-blanc");

            listPotion = new List<Texture2D>();
            listPotion.Add(content.Load<Texture2D>("img/Potion-vide"));
            listPotion.Add(content.Load<Texture2D>("img/Potion-bleu"));
            listPotion.Add(content.Load<Texture2D>("img/Potion-orange"));
            listPotion.Add(content.Load<Texture2D>("img/Potion-violet"));
        }
    }
}
