﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace MonJeu2D {
    
    public class Game1 : Game {

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Player player;

        List<Wall> walls;
        bool clickDown = false;

        Inventory inventory;
        Item item;

        public Game1() {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1920;
            graphics.PreferredBackBufferHeight = 1080;

            this.Window.Title = "Mon petit jeu";
            this.IsMouseVisible = true;
            graphics.IsFullScreen = false;
            Content.RootDirectory = "Content";
        }

       
        protected override void Initialize() {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

       
        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // TODO: use this.Content to load your game content here

            player = new Player(2);
            Ressource.LoadContent(Content);

            walls = new List<Wall>();
            inventory = new Inventory();
            item = new Item(2);

        }

        
        protected override void UnloadContent() {
            // TODO: Unload any non ContentManager content here
        }

        
        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
           
            player.Update(Mouse.GetState(), Keyboard.GetState(), walls);

            foreach (Wall item in walls) {
                item.Update(Mouse.GetState(), Keyboard.GetState());
            }
            if (Keyboard.GetState().IsKeyDown(Keys.LeftControl) && Mouse.GetState().LeftButton == ButtonState.Pressed && !clickDown) {
                walls.Add(new Wall(Mouse.GetState().X, Mouse.GetState().Y, Ressource.pixel, 25, Color.Red));
                clickDown = true;
            }
            if (Keyboard.GetState().IsKeyUp(Keys.LeftControl) && Mouse.GetState().LeftButton == ButtonState.Released && clickDown) {
                clickDown = false;
            }

            base.Update(gameTime);

        }

        
        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            player.Draw(spriteBatch);

            foreach (Wall item in walls) {
                item.Draw(spriteBatch);
            }

            inventory.Draw(spriteBatch);
            item.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
