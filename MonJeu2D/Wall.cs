﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonJeu2D {
    class Wall {
        //CHAMPS
        public Rectangle hitbox;
        public Texture2D textureWall;
        public Color color;

        //CONSTRUCTEUR
        public Wall(int X, int Y, Texture2D TEXTUREWALL, int size, Color COLOR) {
            this.textureWall = TEXTUREWALL;
            this.color = COLOR;
            this.hitbox = new Rectangle(X, Y, size, size);
        }

        //METHODE

        //UPDATE & DRAW
        public void Update(MouseState mouse, KeyboardState keyboard) {

        }

        public void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(this.textureWall, this.hitbox, this.color);
        }
    }
}
